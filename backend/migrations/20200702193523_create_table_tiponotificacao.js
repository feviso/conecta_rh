exports.up = function (knex, Promise) {
    return knex.schema.createTable("tiponotificacao", (table) => {
      table.increments("id").primary();
      table.string("descricao").notNull();
    });
  };
  
  exports.down = function (knex, Promise) {
    return knex.schema.dropTable("tiponotificacao");
  };
  