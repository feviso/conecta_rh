exports.up = function (knex, Promise) {
    return knex.schema.createTable("noticias", (table) => {
      table.increments("id").primary();
      table.string("titulo").notNull();
      table.string("resumo").notNull();
      table.string("destino").notNull();
      table.string("conteudo").notNull();
      table.string("image", 1000);
      table.datetime("datacadastro").notNull();
      table.integer("idtipo").references("id").inTable("tiponotificacao").notNull();
    });
  };
  
  exports.down = function (knex, Promise) {
    return knex.schema.dropTable("noticias");
  };
  