// Update with your config settings.

module.exports = {
  client: "postgresql",
  connection: {
    database: "dbrh",
    user: "postgres",
    password: "042023",
  },
  pool: {
    min: 2,
    max: 10,
    propagateCreateError: false, // <- default is true, set to false
  },
  migrations: {
    tableName: "knex_migrations",
  },
};
