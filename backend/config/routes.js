module.exports = app => {
    //ROTAS MOBILE
    app.post('/signup', app.api.user.save)
    app.post('/signin', app.api.auth.signin)

/*     app.route('/tasks')
        .all(app.config.passport.authenticate())
        .get(app.api.task.getTasks)
        .post(app.api.task.save)
        
    app.route('/tasks/:id')
        .all(app.config.passport.authenticate())
        .delete(app.api.task.remove)

    app.route('/tasks/:id/toggle')
        .all(app.config.passport.authenticate())
        .put(app.api.task.toggleTask) */
            
    // ROTAS WEB
    app.route('/noticias')
        .get(app.api.noticias.getNoticias)
        .post(app.api.noticias.save)
    app.route('/noticias/:id')
        .delete(app.api.noticias.remove)

    app.route('/tiponoticia')
        .post(app.api.tiponoticia.save)
        .get(app.api.tiponoticia.getTipos)

    app.route('/tiponoticia/:id')
        .delete(app.api.tiponoticia.remove)
    
    // SEND EMAIL
    app.route('/sendMail')
        .get(app.api.sendMailer.enviarEmail)
        
}
