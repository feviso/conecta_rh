const express = require('express')
const app = express()
const db = require('./config/db')
const consgin = require('consign')

//
consgin()
    .include('./config/passport.js')
    .then('./config/middlewares.js')
    .then('./api')
    .then('./config/routes.js')
    .into(app)

app.db = db

// Servidor levantado
app.listen(3333, () =>{
    console.log('Backend executando Port(3333)...')
})


