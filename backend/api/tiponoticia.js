module.exports = (app) => {
  
  const save = (req, res) => {
    //Inserindo dados
    app.db("tiponotificacao")
      .insert(req.body)
      .then((_) => res.status(204).send())
      .catch((err) => res.status(400).json(err));
  };

  const getTipos = (req, res) => {
    app.db("tiponotificacao")
      .then((tipo) => res.json(tipo))
      .catch((err) => res.status(400).json(err));
  };

  const remove = (req, res) => {
    app.db('tiponotificacao')
        .where({id: req.params.id})
        .del()
        .then(rowsDelete => {
            if(rowsDelete > 0){
                res.status(204).send()
            }else {
                const msg = `Dados não encontrado para o id: ${req.params.id}.`
                res.status(400).send(msg)
            }
        })
        .catch(err => res.status(400).json(err))
}

  return { remove, save, getTipos };
};
