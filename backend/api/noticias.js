/* import moment from 'moment'
const date = req.query.date ? req.query.date
: moment().endOf('day').toDate() */

module.exports = (app) => {

  const save = (req, res) => {
    //Inserindo dados
    app
      .db("noticias")
      .insert({
        titulo: req.body.titulo,
        resumo: req.body.resumo,
        destino: req.body.destino,
        conteudo: req.body.conteudo,
        image: req.body.imagem,
        datacadastro: req.body.datacadastro,
        idtipo: req.body.idtipo
      })
      .then((_) => res.status(204).send())
      .catch((err) => res.status(400).json(err));
  };

  const remove = (req, res) => {
    app.db('noticias')
        .where({id: req.params.id})
        .del()
        .then(rowsDelete => {
            if(rowsDelete > 0){
                res.status(204).send()
            }else {
                const msg = `Dados não encontrado para o id: ${req.params.id}.`
                res.status(400).send(msg)
            }
        })
        .catch(err => res.status(400).json(err))
  }

  const getNoticias = (req, res) => {
    app
      .db("noticias")
      .orderBy("datacadastro")
      .then((noticias) => res.json(noticias))
      .catch((err) => res.status(400).json(err));
  };
  return { save, remove, getNoticias };
};
