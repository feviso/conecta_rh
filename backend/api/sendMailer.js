const nodemailer = require("nodemailer");

module.exports = (app) => {

    const enviarEmail = () => {
        let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false,
            auth: {
                user: "valdeirpereiramachado@gmail.com",
                pass: "Apex042023@",
            },
        });

        transporter.sendMail({
                from: "valdeirpereiramachado@gmail.com",
                to: "valdeirpereira0420@gmail.com",
                subject: "App Conecta RH informa",
                text: "App Conecta RH informa",
                html: `
		<div>
			<div><strong> APP CONECTA RH </strong></div>
			<hr/>
                <table width="100%">
                        <thead>
                            <tr>
                                <th>Você tem uma nova notificação do App Conecta RH...!</th>
                            </tr>
                        </thead>
                    <tbody>
                        <td> Estou recebendo minhas notificações.!</td>
                    </tbody>
                </table>
        </div>
        `,
            })
            .then((msg) => {
                console.log("Sucesso! ", msg);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    return { enviarEmail };
};

/*
<html>
<head>
<meta charset="utf-8"/>
<title>Teste Email</title>
<style>
		hr {
				border: 0; height: 1px;
				background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
		}
		table
		{
				font-family: arial, sans-serif;border-collapse: collapse;
				width:100%;
		}
		th, td
		{
				border: 1px solid #dddddd;
				padding: 3px;
		}
		tr:nth-child(even)
		{
				background-color: #dddddd;
		}
</style>
</head>
<body>
		<a href="http://www.hss.com.br/" target="_blank">
				<img alt="Logo HSS Informática" title="Logo HSS informática" style="margin-left:5%; float:left"  src="http://www.hss.com.br/images/logo_oficial.png"></img>
		</a>
		<div style="margin-left:15%;margin-right:15%;text-align:center; margin-top:1%">
				<p style="font-size:30px;font-family:Verdana"><b><I>INFORMATIVO DE SUPRIMENTOS</I></b></p>
		</div>
		<div style="background-color:#4682B4;text-align:center;height:55px;margin-left:5%;margin-right:5%;margin-top:5px">
				<p style="font-size:25px; color:white; font-family:Verdana;height:7px"><B>NOME CLIENTE</B></p>
				<p style="font-size:15px; color:white; font-family:Verdana;">Dados cliente</p>
		<table>
				<tr>
						<th width="33%">PEDIDO: <i>Nº PEDIDO</i></th>
						<th width="33%">SUPRIMENTOS SOLICITADOS</th>
						<th width="33%">PREVISÃO DE ENTREGA: <i>dd/MM/yyyy</i></th>
				</tr>
		</table>
		<table>
				<tr>
						<th width="33%" align="center">Suprimento</th>
						<th width="33%" align="center">Quantidade</th>
						<th width="33%" align="center">Impressoras compatíveis</th>
				</tr>
				<tr>
						<td align="center">Modelo 01</td>
						<td align="center">01</td>
						<td align="center">MX511</td>
				</tr>
				<tr>
						<td align="center">Modelo 02</td>
						<td align="center">02</td>
						<td align="center">MX611</td>
				</tr>
				<tr>
						<td align="center">Modelo 03</td>
						<td align="center">03</td>
						<td align="center">MX611</td>
				</tr>
		</table>
		<br>
		<hr>
		<div style="background-color:#4682B4;text-align:center;height:25px;">
				<p style="font-size:18px; color:white; font-family:Verdana;height:7px">SUPRIMENTOS AGUARDANDO RETORNO</p>
		<br>
		<table>
				<tr>
						<th width="50%">Modelo de Suprimento</th>
						<th width="50%">Quantidade</th>
				</tr>
				<tr>
						<td align="center">Modelo 03</td>
						<td align="center">05</td>
				</tr>
				<tr>
						<td align="center">Modelo 04</td>
						<td align="center">04</td>
				</tr>
		</table>
		<br>
		<hr>
		<p style="text-align:center; font-family:Verdana; font-size:12px;">Esse <b>email foi enviado automaticamente</b> pelo sistema,<b> não é necessário respondê-lo.</b><br> Em caso de dúvidas responda este email ou entre em contato pelo telefone <b><i>(51)3594-7561</i></b>.</p>
		<div style="background-color:#4682B4;text-align:center;height:22px;">
				<p style="font-size:15px; color:white; font-family:Verdana;height:7px">HSS Assessoria em Informática - Rua 25 de Julho, 917 - Rio Branco - Novo Hamburgo / RS</p>
		</div>
		</div>
		</div>
</body>
</html> */