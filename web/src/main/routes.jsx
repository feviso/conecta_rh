import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Home from '../components/home/home'
import UserCrud from '../components/user/UserCrud'
import Noticias from '../components/noticias/noticia'
import CriarNoticia from '../components/noticias/criarNoticia/criarNoticia'
import Agenda from '../components/noticias/agenda/agenda'
import Automatizar from '../components/noticias/automatizar/automatizar'
import Destinatarios from '../components/noticias/destinatario/destinatario'
import Relatorios from '../components/noticias/relatorios/relatorios'

import Beneficios from '../components/beneficios/beneficios'
import Parceiros from '../components/parceiro/parceiro'
import Oportunidades from '../components/oportunidades/oportunidade'
import Duvidas from '../components/duvidas/duvidas'

export default props =>

    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/noticias" component={Noticias} />
        <Route path="/criar-noticia" component={CriarNoticia} />
        <Route path="/agenda" component={Agenda} />
        <Route path="/relatorios" component={Relatorios} />
        <Route path="/automatizar-agenda" component={Automatizar} />
        <Route path="/destinatarios" component={Destinatarios} />
        <Route path="/parceiros" component={Parceiros} />
        <Route path="/oportunidades" component={Oportunidades} />
        <Route path="/duvidas" component={Duvidas} />
        <Route path="/beneficios" component={Beneficios} />
        <Route path="/users" component={UserCrud} />
        <Redirect from='*' to='/' />
    </Switch>