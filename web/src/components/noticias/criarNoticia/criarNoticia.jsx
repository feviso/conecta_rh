import React, { Component } from 'react'
import axios from 'axios'
import Main from '../../template/Main'
import Grid from '../../template/grid'
import Span from '../../template/span'
import './criarNoticia.css'
import './editor.css'
import api from '../../../service/api'

const headerProps = {
	icon: 'edit',
	title: 'Criar minhas Notícias',
	subtitle: 'Gerenciar minhas notícias'
}

const ShowTipos = (props) => {
	return (
		<option
			value={props.tipo.descricao} value={props.tipo.id}>
			{props.tipo.descricao}
		</option>)
}

export default class CriarNoticia extends Component {

	//Peermite passar e pegar os parametros pra outros componentes
	constructor(props) {
		super(props)
		this.state = {
			titulo: '',
			resumo: '',
			conteudo: '',
			list: [],
			tiposArray: [],
			selectItemTipo: '',
			selectItemDestino: '',
			tipodescricao: '',
			id: ''
		}
		this.addChange = this.addChange.bind(this)
		this.cadastrarNoticia = this.cadastrarNoticia.bind(this)
		this.addChangeTitulo = this.addChangeTitulo.bind(this)
		this.addChangeResumo = this.addChangeResumo.bind(this)
		this.addChangeConteudo = this.addChangeConteudo.bind(this)
		this.addTipo = this.addTipo.bind(this)
		this.onChangeComboDestino = this.onChangeComboDestino.bind(this)
		this.onChangeComboTipo = this.onChangeComboTipo.bind(this)
		this.refresh()
	}

	enviarEmail() {
		api.get('sendMail')
			.then(function (response) {
				console.log(response);
			})
			.catch(function (error) {
				console.log(error);
			})
	}

	cadastrarNoticia() {
		console.log('Add...')

		//Integrando - Inserindo dados
		let _img = 'link da imagem'
		let _conteudo = 'Conteudo'

		let currentDate = new Date()

		const titulo = this.state.titulo
		const datacadastro = currentDate
		const idtipo = parseInt(this.state.selectItemTipo)
		const resumo = this.state.resumo
		const destino = this.state.selectItemDestino
		const conteudo = _conteudo
		const imagem = _img

		api.post('noticias',
			{
				titulo,
				resumo,
				destino,
				conteudo,
				imagem,
				datacadastro,
				idtipo
			})
			//.then(resposta => this.enviarEmail())
	}

	//Lista os Tipo cadastrados na base de dados
	getTipos() {
		return this.state.tiposArray.map((tipodescricao, id) => {
			return <ShowTipos tipo={tipodescricao} key={id} />
		});
	}

	//Carrega os componente Combo quando carrega a tela
	componentDidMount() {
		api.get('tiponoticia')		
			.then(tipo => {
				this.setState({
					tiposArray: tipo.data
				});
			}).catch(err => {
				console.log(err);
			});
	}

	addChange(e) {
		this.setState({
			...this.state, tipodescricao: e.target.value
		})
	}

	addChangeTitulo(e) {
		this.setState({
			...this.state, titulo: e.target.value,
		})
		console.log(this.state.titulo)
	}
	addChangeResumo(e) {
		this.setState({
			...this.state, resumo: e.target.value,
		})
		console.log(this.state.resumo)
	}
	addChangeConteudo(e) {
		this.setState({ ...this.state, conteudo: e.target.value })
		console.log(this.state.conteudo)
	}

	refresh() {
		//Recuperando dados e adicionando na lista
		api.get('noticias')
			.then(resposta => this.setState({ ...this.state, tipodescricao: '', list: resposta.data }))
	}

	addTipo() {
		const descricao = this.state.tipodescricao
		api.post('tiponoticia', { descricao })
			.then(resposta => this.refresh() /* console.log('Funcou') */)
	}

	onChangeComboDestino(e) {
		this.setState({ selectItemDestino: e.target.value });
	}
	onChangeComboTipo(e) {
		this.setState({ selectItemTipo: e.target.value });
	}

	render() {
		return (
			<Main {...headerProps}>

				<Grid cols="12 9 12">
					<div className="mb-3 text-right" >
						<button type="submit" className="btn btn-default btn-sm">
							<i className="fa fa-edit"></i>&nbsp;Terminar depois
                   	 	</button>
						<button type="submit" className="btn btn-default btn-sm">
							<i className="fa fa-calendar"></i>&nbsp;Agendar
                    	</button>
						<button onClick={this.cadastrarNoticia} className="btn btn-default btn-sm">
							<i className="fa fa-send"></i>&nbsp;Enviar/Salvar
                    	</button>
						{/* <IconButton icon='edit' name="Terminar depois" onClick={}  />
						<IconButton icon='calendar' name="Agendar"  onClick={}  />
						<IconButton icon='send' name="Enviar"  onClick={}  /> */}
					</div>
				</Grid>

				<Grid cols="12 9 12">
					<div className="card mb-1">
						<div className="card-header">
							<div className="mb-0 btn-header" data-toggle="collapse" data-target="#collapseDestino" aria-expanded="true" aria-controls="collapseOne">
								<span className="fa fa-users"> Destinatário - Tipo</span>
								<i className="fa fa-caret-down"> Adicionar destinatário e tipo</i>
							</div>
						</div>
						<div id="collapseDestino" className="collapse">
							<div className="card-body row">
								<Grid cols="12 9 5">
									<label htmlFor="destinatario">Destinatário</label>
									<div className="listForm">
										<form>
											<select className="form-control form-control-sm" value={this.state.selectItemDestino} onChange={this.onChangeComboDestino} name="destinatario" id="destinatario">
												<option>Selecione o destinatário...</option>
												<option>Todos</option>
												<option>Gestores</option>
												<option>Docentes</option>
												<option>Técnicos Administrativos</option>
												<option>Jovem Aprendiz</option>
												{/* {this.getDestinatarios()} */}
											</select>
										</form>
									</div>
								</Grid>
								<Grid cols="12 9 3">
									<label htmlFor="tipo">Tipo da notícia</label>
									<div className="listForm">
										<form>
											<select className="form-control form-control-sm" value={this.state.selectItemTipo} onChange={this.onChangeComboTipo} name="idTipo" id="idTipo">
												<option value="" disabled selected>Selecione o tipo...</option>
												{this.getTipos()}
											</select>
										</form>
									</div>
								</Grid>
								<Grid cols="12 9 4">
									<div className="card-header">
										<div className="mb-0 btn-header" data-toggle="collapse" data-target="#multiCollapseTipo" aria-expanded="true" aria-controls="collapseOne">
											<span className="fa fa-plus"> Cadastrar tipo</span>
											<i className="fa fa-caret-down"></i>
										</div>
									</div>
									<div className="card-body">
										<div className="collapse multi-collapse" id="multiCollapseTipo">
											<div className="card">
												<div className="card-body">
													<form className="form-group row">
														<Grid cols="12 9 8">
															<input
																type="text"
																className="form-control form-control-sm"
																id="tipodescricao"
																placeholder="Descrição do tipo"
																onChange={this.addChange}
																value={this.tipodescricao} />
														</Grid>
														<Grid cols="12 3 4">
															<button type="submit" className="btn btn-default btn-sm" onClick={this.addTipo}>
																<i className="fa fa-plus"></i>&nbsp;Incluir
                   	 										</button>

															{/* <IconButton icon='plus' name="Incluir" onClick={this.addTipo} /> */}
														</Grid>
													</form>
												</div>
											</div>
										</div>
									</div>
								</Grid>
								<Grid cols="12 9 12">
									<div className="text-right">
										<button type="submit" className="btn btn-default btn-sm" /* onClick={this.addTipo} */>
											<i className="fa fa-save"></i>&nbsp;Salvar
                   	 					</button>
										{/* <IconButton style="primary" icon='plus' name="Salvar" onClick={props.addTipo} /> */}
									</div>
								</Grid>
							</div>
						</div>
					</div>
					{/* <div className="card mb-1">
						<div className="card-header">
							<div className="mb-0 btn-header" data-toggle="collapse" data-target="#collapseTipo" aria-expanded="true" aria-controls="collapseOne">
								<span className="fa fa-check"> Tipo:</span>
								<i className="fa fa-caret-down"> Adicionar tipo</i>
							</div>
						</div>
						<div id="collapseTipo" className="collapse">
							<div className="card-body">
								<div>
									Tipo
									<Grid cols="12 3 4">
										<IconButton style="primary" icon='save' name="Salvar" onClick={props.addTipo} />
									</Grid>
								</div>
							</div>
						</div>
					</div> */}
					<div className="card mb-1">
						<div className="card-header">
							<div className="mb-0 btn-header" data-toggle="collapse" data-target="#collapseTema" aria-expanded="true" aria-controls="collapseOne">
								<span className="fa fa-align-justify"> Tema - Conteúdo</span>
								<i className="fa fa-caret-down"> Adicionar tema e conteúdo</i>
							</div>
						</div>
						<div id="collapseTema" className="collapse">
							<div className="card-body row">
								<Grid cols="12 9 12">
									<label htmlFor="titulo">Título</label>
									<form className="form-group">
										<input
											type="text"
											className="form-control form-control-sm"
											id="titulo"
											placeholder="Adicione um título"
											onChange={this.addChangeTitulo}
											titulo={this.state.titulo} />

									</form>
								</Grid>
								<Grid cols="12 9 12">
									<label htmlFor="resumo">Resumo</label>
									<form className="form-group">
										<textarea
											rows="2" maxLength="100"
											type="text"
											className="form-control form-control-sm mb-4"
											id="resumo"
											placeholder="Adicione um breve resumo..."
											onChange={this.addChangeResumo}
											value={this.state.resumo} />
									</form>
								</Grid>
								<Grid cols="12 9 12">

									{/* EDITOR */}
									<div className="row justify-content-center">
										<Grid cols="12 10 6" >
											<div>
												<Span name="Meu Editor" size="30" icon="edit" />
												<textarea
													name="editor"
													id="editor"
													onChange={this.addChangeConteudo}
													value={this.state.conteudo} 
													cols="20"
													rows="5">
												</textarea>
											</div>
											{/* <div className="actions">
						<IconButton style="primary" icon='save' name="Salvar" onClick={props.addTipo}/>
					</div> */}
										</Grid>
										<Grid cols="12 10 6">
											<div>
												<Span name="Preview" size="20" icon="eye" />
											</div>
											<div>
												{/* <label className="justify-content-center" for="img">Carregar uma Logo:</label> */}
												<label className='label-file' for='selecao-arquivo'>
													<Span icon="image" /> Selecionar uma logo
												</label>
												<input type="file" id="selecao-arquivo" className="form-group" name="carrega" accept="image/png, image/jpeg" />
											</div>

											<img className="mt-2" id="img" width="180px" />

											<div className="conteudo mt-2">
												<label className="mt-3" for="img">
													Descrição do conteúdo <br /> <br />

													Descrição 1<br />
													Descrição 2<br />
													Descrição 3<br />
													Descrição 4<br />
													Descrição 5<br />

												</label>
											</div>
										</Grid>

									</div>

									{/* <Editor 
										addChangeConteudo={this.addChangeConteudo} 
										conteudo={this.state.conteudo} /> */}

								</Grid>
								<Grid cols="12 9 12">
									<div className="mt-3">
										<div className="text-right">
											<button type="submit" className="btn btn-default btn-sm" /* onClick={this.addTipo} */>
												<i className="fa fa-save"></i>&nbsp;Salvar
                   	 					</button>
											{/* <IconButton style="primary" icon='plus' name="Salvar" onClick={props.addTipo} /> */}
										</div>
									</div>
								</Grid>
							</div>
						</div>
					</div>
					{/* <div className="card mb-1">
						<div className="card-header">
							<div className="mb-0 btn-header" data-toggle="collapse" data-target="#collapseConteudo" aria-expanded="true" aria-controls="collapseOne">
								<span className="fa fa-file"> Conteúdo</span>
								<i className="fa fa-caret-down"> Adicionar conteúdo</i>
							</div>
						</div>
						<div id="collapseConteudo" className="collapse">
							<div className="card-body">
								<div>
									<Editor />
									<Grid cols="12 3 4">
										<IconButton style="primary" icon='save' name="Salvar" onClick={props.addTipo} />
									</Grid>
								</div>
							</div>
						</div>
					</div> */}
				</Grid>
			</Main>
		)
	}
}