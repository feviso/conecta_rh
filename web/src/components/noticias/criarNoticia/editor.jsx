import React, { Component } from "react";
import "./editor.css";
import Grid from '../../template/grid'
//import IconButton from '../../template/iconButton'
import Span from '../../template/span'

export default props => {

	return (
		<div className="row justify-content-center">
			<Grid cols="12 10 6" >
				<div>
					<Span name="Meu Editor" size="30" icon="edit" />
					<textarea 
						name="editor" 
						id="editor" 
						onChange={props.addChangeConteudo} 
						value={props.conteudo} 
						cols="20" 
						rows="5">
					</textarea>
				</div>
				{/* <div className="actions">
						<IconButton style="primary" icon='save' name="Salvar" onClick={props.addTipo}/>
					</div> */}
			</Grid>
			<Grid cols="12 10 6">
				<div>
					<Span name="Preview" size="20" icon="eye" />
				</div>
				<div>
					{/* <label className="justify-content-center" for="img">Carregar uma Logo:</label> */}
					<label className='label-file' for='selecao-arquivo'>
						<Span icon="image" /> Selecionar uma logo
						</label>
					<input type="file" id="selecao-arquivo" className="form-group" name="carrega" accept="image/png, image/jpeg" />
				</div>

				<img className="mt-2" id="img" width="180px" />

				<div className="conteudo mt-2">
					<label className="mt-3" for="img">
						Descrição do conteúdo <br /> <br />

							Descrição 1<br />
							Descrição 2<br />
							Descrição 3<br />
							Descrição 4<br />
							Descrição 5<br />

					</label>
				</div>
			</Grid>

		</div>
	)
}
