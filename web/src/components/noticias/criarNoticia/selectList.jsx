import React from 'react'

export default props => {

    return (
        <div className="listForm">
        <form>
            <select className="form-control" value={this.state.selectItem} onChange={props.onChangeCombo} name="idTipo" id="idTipo">
                <option>Selecione o tipo...</option>
                {this.getTipos()}
            </select>
        </form>
    </div>
    )
}
