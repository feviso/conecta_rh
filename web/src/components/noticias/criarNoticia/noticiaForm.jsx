import React from 'react'
import Grid from '../../template/grid'
import IconButton from '../../template/iconButton'

export default props => {

    //Criando teclas de Atalho para o App
    const telhasAtalhos = (e) => {
        if (e.key === 'Enter') {
            e.shiftKey ? props.handlePesquisar() : props.cadastrarNoticia()
        } else if (e.key === 'Escape') {
            props.handleClear()
        }
    }//Fim 

    return (
        <div role="form" className="noticiaForm">
            <form>
                <Grid cols="12 9 12">
                    <label htmlFor="">Descrição da notícia</label>
                </Grid>
                <Grid cols="12 9 12">
                    <input
                        id="descricao"
                        className="form-control"
                        placeholder='Digite a notificação'
                        onChange={props.handleChange}
                        onKeyUp={telhasAtalhos}
                        value={props.descricao} />
                </Grid>
                <Grid cols="12 9 12">
                    <IconButton style='primary' icon='plus' onClick={props.cadastrarNoticia} name="Cadastrar" />
                    <IconButton style='info' icon='search' onClick={props.handlePesquisar} name="Pesquisar" />
                    <IconButton style='default' icon='undo' onClick={props.handleClear} name="Limpar" />
                </Grid>
            </form>
        </div >

    )
}