import React from 'react'
//import IconButton from '../template/iconButton'
import Grid from '../../template/grid'
import Span from '../../template/span'
import moment from 'moment'
import './noticia.css'

export default props => {

    const carregarTabela = () => {
        const list = props.list || []
        return list.map(noticia => (
            <tr key={noticia.id}>
                <td>{noticia.id}</td>
                <td>{noticia.titulo}</td>
                <td>{noticia.resumo}</td>
                <td>{noticia.conteudo}</td>
                <td>{noticia.image}</td>
                <td>
                    {
                        noticia.idtipo === 1
                            ? 'Ceub informa'
                            : noticia.idtipo === 2
                                ? 'RH informa'
                                : noticia.idtipo === 3
                                    ? 'Eventos'
                                : noticia.idtipo === 4
                                    ? 'Portaria'
                                    : ''
                    }

                </td>
                <td>{moment(noticia.datacadastro).locale('pt-br').format(`L LT`)}</td>
                <td>500/700</td>
                {/*   <td>
                    <IconButton style="success" icon="edit" name="Editar"  onClick={() => props.handleEdit(noticia)} />
                    <IconButton style="danger" icon="trash-o" name="Excluir"  onClick={() => props.handleRemove(noticia)} />
                    <IconButton style="primary" icon="send" name="Enviar"  onClick={() => props.handleSend(noticia)} />
                </td> */}
            </tr>
        ))
    }

    return (
        <div className="row">
            <Grid cols="12 9 12">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <strong><Span icon="th-list" size={25} />&nbsp;&nbsp; Notícias Cadastradas</strong>
                    </div>
                    <div className="table-responsive">
                        <table className="table table-size table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Título</th>
                                    <th>Resumo</th>
                                    <th>Destinatário</th>
                                    <th>Tipo de Notícia</th>
                                    <th>Destinatário</th>
                                    <th>Data/Hora Enviada</th>
                                    <th>Total de aberturas</th>
                                    {/* <th className="tableActions">Ações</th> */}
                                </tr>
                            </thead>
                            <tbody>
                                {carregarTabela()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </Grid>
        </div>

    )
}