import React, { Component } from 'react'
import Main from '../../template/Main'
import Span from '../../template/span'
import lista from '../../../assets/lista.json'
import './destinatario.css'

const headerProps = {
    icon: 'send',
    title: 'Destinatários',
    subtitle: 'Minha Lista de destinatários'
}

export default class Destinatarios extends Component {

    renderTable() {
        return (
            <table className="table mt-4">
                <thead>
                    <tr>
                        <th><Span icon="check" /></th>
                        <th>E-mail</th>
                        <th>Nome</th>
                        <th>Sobrenome</th>
                        <th>Lista de E-mail</th>
                    </tr>
                </thead>
                <tbody>
                    {lista.map((item) => {
                        return (
                            <tr key={item.id}>
                                <td>
                                    <div className="form-check">
                                        <input type="checkbox" className="form-check-input" />
                                    </div>
                                </td>
                                <td>{item.email}</td>
                                <td>{item.nome}</td>
                                <td>{item.sobrenome}</td>
                                <td>{item.listaEmail}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }
    render() {
        return (
            <Main {...headerProps}>
                <Span size={25} icon="send" name="Destinatários" />
                {this.renderTable()}
            </Main>

        )
    }
}