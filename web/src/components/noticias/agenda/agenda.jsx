import React, { Component } from 'react'
import Main from '../../template/Main'
//import Span from '../../template/span'
import Imagem from '../../../assets/imgs/agenda.png'

const headerProps = {
    icon: 'calendar',
    title: 'Agenda',
    subtitle: 'Minhas notícias agendadas'
}

export default class Agenda extends Component {

    render() {
        return (
            <Main {...headerProps}>
                {/* <Span size={25} icon="calendar" name="Agenda" /> */}
                <div>
                    <img src={Imagem} alt="Agenda" />
                </div>
            </Main>
        )
    }
}