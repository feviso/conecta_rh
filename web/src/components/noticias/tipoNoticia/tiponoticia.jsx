import React, { Component } from 'react'
import axios from 'axios'

import TipoForm from './tipoForm'
import TipoList from './tipoList'
import Main from '../../template/Main'
import api from '../../../service/api'

const headerProps = {
	icon: 'edit',
	title: 'Cadastrar minhas Notícias',
	subtitle: 'Meu Dashboard de Notícias'
}

//Base de Dados 
const URL = 'http://localhost:3000/tiponoticia'

export default class TipoNoticia extends Component {

	constructor(props) {
		super(props)
		this.state = { tipodescricao: '', list: [], id: '' }

		this.addChange = this.addChange.bind(this)
		this.addTipo = this.addTipo.bind(this)
		this.removeTipo = this.removeTipo.bind(this)
		this.refresh()
	}

	addChange(e) {
		this.setState({ ...this.state, tipodescricao: e.target.value })
	}

	refresh() {
		//Recuperando dados e adicionando na lista
		api.get('tiponoticia')
			.then(resposta => this.setState({ ...this.state, tipodescricao: '', list: resposta.data }))
	}

	addTipo() {
		const descricao = this.state.tipodescricao
		api.get('tiponoticia', { descricao })
			.then(resposta => this.refresh() /* console.log('Funcou') */)
	}
	removeTipo(tipo) {
		api.delete(`tiponoticia/${tipo.id}`)
			.then(resposta => this.refresh())
	}

	render() {
		return (
			<Main {...headerProps}>
				<div>
					<TipoForm
						tipodescricao={this.state.tipodescricao}
						addChange={this.addChange}
						addTipo={this.addTipo} />
					{/* <TipoList list={this.state.list} removeTipo={this.removeTipo} /> */}
				</div>
			</Main>
		)
	}
}