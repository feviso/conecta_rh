import React from 'react'
import Grid from '../../template/grid'
import IconButton from '../../template/iconButton'

export default props => {
    return (
        <div role="form" className="tipoForm">

            <fieldset className="well">
                <legend className="well-legend">Cadastrar novo tipo de Notícia</legend>
                <Grid cols="12 9 10">
                    <label htmlFor="desc">Descrição do tipo</label>
                </Grid>
                <Grid cols="12 9 4">
                    <input
                        id="tipodescricao"
                        className="form-control"
                        placeholder="Adicionar novo tipo..."
                        onChange={props.addChange}
                        value={props.tipodescricao} />
                </Grid>
                <div className="mt-2">
                    <Grid cols="12 3 4">
                        <IconButton style="primary" icon='plus' name="Cadastrar"
                            onClick={props.addTipo} />
                    </Grid>
                </div>
            </fieldset>
        </div>
    )
}