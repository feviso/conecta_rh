import React, { Component } from 'react'
import api from '../../../service/api'
//Base de Dados 
const URL = 'http://localhost:3000/tiponoticia'

const ShowTipos = (props) => {
    return (
        <option
            value={props.tipo.descricao}>
            {props.tipo.descricao}
        </option>)
};

export default class ListaTipos extends Component {

    constructor(props) {
        super(props)
        this.state = {
            value: '',
            tiposArray: [],
            tipo: '',
            idtipo: ''
        }

    }
    //Carrega os componente Combo quando carrega a tela
    componentDidMount() {
        api.get('tiponoticia')
            .then(tipo => {
                console.log(tipo.data);
                this.setState({
                    tiposArray: tipo.data
                });
            }).catch(err => {
                console.log(err);
            });
    }

    //Lista os Tipo cadastrados na base de dados
    getTipos() {
        return this.state.tiposArray.map((tipodescricao, id) => {
            return <ShowTipos tipo={tipodescricao} key={id} />
        });
    }

    render() {
        return (
            <div className="listForm">
                {/* <label>Selecione o tipo:</label> */}
                <form onSubmit={this.handleSubmit}>
                    <select className="form-control" name="tipo" id="tipo" value={this.state.id} onChange={this.handleChange}>
                        <option>Selecione o tipo...</option>
                        {this.getTipos()}
                    </select>
                </form>
            </div>
        )
    }
}