import React from 'react'
import IconButton from '../../template/iconButton'
import Grid from '../../template/grid'
import Span from '../../template/span'

export default props => {
    //Função para carregar a lista de tipos
    const carregarTipos = () => {
        //Lista setada ou nao
        const list = props.list || []

        return list.map(tipo => (
            <tr key={tipo.id}>
                <td>{tipo.id}</td>
                <td>{tipo.descricao}</td>
                <td>
                    <IconButton style="success" icon="edit" /* name="Editar" */ />
                    <IconButton style="danger" icon="trash-o" /* name="Excluir" */ onClick={() => props.removeTipo(tipo)} />
                </td>
            </tr>
        ))
    }
    return (
        <div className="row">
            <Grid cols="12 9 12">
                <div className="panel panel-default">
                    <div className="panel-heading"><strong><Span icon="th-list" size={25}/>&nbsp;&nbsp; Tipos de notícias cadastrados</strong></div>
                    <table className="table table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição do tipo</th>
                                <th className="tableActions">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            {carregarTipos()}
                        </tbody>
                    </table>
                </div>
            </Grid>
        </div>
    )
}