import React, { Component } from 'react'
import Main from '../../template/Main'
import Span from '../../template/span'

const headerProps = {
    icon: 'cogs',
    title: 'Automatizar Agenda',
    subtitle: 'Agendas automáticas'
}

export default class Automatizar extends Component {

    render() {
        return (
            <Main {...headerProps}>
                <Span size={25} icon="cogs" name="Automatizar" />
            </Main>
        )
    }
}