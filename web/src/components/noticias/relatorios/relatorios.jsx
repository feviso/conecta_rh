import React, { Component } from 'react'
import Main from '../../template/Main'
import Span from '../../template/span'

const headerProps = {
    icon: 'copy',
    title: 'Relatórios',
    subtitle: 'Meus Relatórios'
}

export default class Relatorios extends Component {

    render() {
        return (
            <Main {...headerProps}>
                <Span size={25} icon="copy" name="Relatórios" />
            </Main>
        )
    }
}