import axios from 'axios'
import React, { Component } from 'react'
import NoticiaList from './noticiaList'
import Main from '../template/Main'
import './noticia.css'
import api from '../../service/api'

const headerProps = {
    icon: 'home',
    title: 'Dashboard',
    subtitle: 'Meu Dashboard de Notícias'
}

const ShowTipos = (props) => {
    return (
        <option
            value={props.tipo.descricao} value={props.tipo.id}>
            {props.tipo.descricao}
        </option>)
};

export default class Noticia extends Component {

    //Peermite passar e pegar os parametros pra outros componentes
    constructor(props) {
        super(props)
        this.state = {
            descricao: '',
            list: [],
            tiposArray: [],
            selectItem: ''
        }

        this.cadastrarNoticia = this.cadastrarNoticia.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
        this.handlePesquisar = this.handlePesquisar.bind(this)
        this.handleClear = this.handleClear.bind(this)
        this.onChangeCombo = this.onChangeCombo.bind(this)

        //Chamando o Metod Refresh
        this.refresh()
    }

    refresh(descricao = '') {
        api.get('noticias')
            .then(resposta => this.setState({ ...this.state, descricao, list: resposta.data }))
    }

    cadastrarNoticia() {
        //Integrando - Inserindo dados
        let currentDate = new Date();

        const descricao = this.state.descricao
        const datacadastro = currentDate
        const idtipo = this.state.selectItem

        api.post('noticias', { descricao, datacadastro, idtipo })
            .then(resposta => this.refresh())
    }
    //Lista os Tipo cadastrados na base de dados
    getTipos() {
        return this.state.tiposArray.map((tipodescricao, id) => {
            return <ShowTipos tipo={tipodescricao} key={id} />
        });
    }

    //Carrega os componente Combo quando carrega a tela
    componentDidMount() {
        api.get('tiponoticia')
            .then(tipo => {
                this.setState({
                    tiposArray: tipo.data
                });
            }).catch(err => {
                console.log(err);
            });
    }

    onChangeCombo(e) {
        this.setState({ selectItem: e.target.value });
    }

    handleChange(e) {
        this.setState({ ...this.state, descricao: e.target.value })
    }

    //Removendo uma tarefa
    handleRemove(noticia) {
        api.delete(`noticias/${noticia.id}`)
            .then(resposta => this.refresh(this.state.descricao))
    }

    handlePesquisar() {
        this.refresh(this.state.descricao)
    }

    handleClear() {
        this.refresh()
    }

    render() {

        return (
            <Main {...headerProps}>
                <div>
                    {/* <NoticiaForm
                        descricao={this.state.descricao}
                        handleChange={this.handleChange}
                        cadastrarNoticia={this.cadastrarNoticia}
                        handlePesquisar={this.handlePesquisar}
                        handleClear={this.handleClear} />
                    <Grid cols="12 9 6">
                        <div className="listForm">
                            <form>
                                <select className="form-control" value={this.state.selectItem} onChange={this.onChangeCombo} name="idTipo" id="idTipo">
                                    <option>Selecione o tipo...</option>
                                    {this.getTipos()}
                                </select>
                            </form>
                        </div>
                    </Grid> */}
                    <NoticiaList
                        list={this.state.list}
                        handleRemove={this.handleRemove} />
                </div>
            </Main>
        )
    }
}