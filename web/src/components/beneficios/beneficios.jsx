import React from 'react'

import Main from '../template/Main'

export default props => 
    <Main icon="usd" title="Contracheques e Benefícios" subtitle="Muito Benefícios pra Você - App Conecta">
        <div className="display-4">Contracheques e Benefícios</div>
        <hr/>
    </Main>