import React from 'react'

import Main from '../template/Main'

export default props => 
    <Main icon="id-card" title="Dúvidas" subtitle="Retire suas dúvidas aqui - App Conecta">
        <div className="display-4">Dúvidas RH e Você</div>
        <hr/>
    </Main>