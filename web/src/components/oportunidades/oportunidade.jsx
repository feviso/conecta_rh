import React from 'react';

import Main from '../template/Main';
import IconButton from '../template/iconButton';
export default props => (
  <Main
    icon="share-alt"
    title="Oportunidades"
    subtitle="Muitas Oportunidades pra Você - App Conecta"
  >
    <a href="https://uniceub.empregare.com/pt-br/vagas" target="_blank" rel="noopener">
      <IconButton style="primary" icon="share-alt" name="&nbsp; Acessar oportunidades" />
    </a>
    <hr />
  </Main>
);
