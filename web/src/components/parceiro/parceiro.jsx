import React from 'react'

import Main from '../template/Main'

export default props => 
    <Main icon="users" title="Parceiros e Vantagens" subtitle="Nossas parcerias - App Conecta">
        <div className="display-4">Parceiros e Vantagens</div>
        <hr/>
    </Main>