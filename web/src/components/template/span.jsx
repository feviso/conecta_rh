import React from 'react'

export default props => {
    
    return (
        <span style={{ fontSize: props.size }}>
            <i className={'fa fa-' + props.icon}> {props.name}</i>
        </span>

    )
}

    



