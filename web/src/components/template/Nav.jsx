import './Nav.css'
import React from 'react'
import NavItem from './navItem'

export default props =>
    <aside className="menu-area">
        <nav className="menu">
            <NavItem link='noticias' icon="home" name="Dashboard" />
            <span className="dropdown-btn"> <i className="fa fa-users"></i>&nbsp;Notícias
                <i className="fa fa-caret-down"></i>
            </span>
            <div className="dropdown-container">
                <NavItem link='criar-noticia' icon="edit" name="&nbsp;Criar Notícia" />
                <NavItem link='destinatarios' icon="send" name="&nbsp;Destinatários" />
                <NavItem link='automatizar-agenda' icon="cogs" name="&nbsp;Automatizar" />
                <NavItem link='agenda' icon="calendar" name="&nbsp;Agenda" />
                <NavItem link='relatorios' icon="copy" name="&nbsp;Relatórios" />
            </div>
            <NavItem link='parceiros' icon="users" name="&nbsp;Parceiro e Vantagens" />
            <NavItem link='oportunidades' icon="share-alt" name=" &nbsp;Oportunidades" />
            
            <NavItem link='duvidas' icon="id-card" name="&nbsp; Dúvidas, HR e Você" />
            <NavItem link='beneficios' icon="usd" name="&nbsp;Contracheque e Benefícios" />
        </nav>
    </aside>
