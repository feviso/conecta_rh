import React from 'react'

export default props => {
    return (
        <button className={'btn btn-sm btn-' + props.style}
            onClick={props.onClick} type={props.type}>
            <i className={'fa fa-' + props.icon}> {props.name}</i>
        </button>

    )
}





