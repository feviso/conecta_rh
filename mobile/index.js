/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Navegacao from './src/Navigator';
//import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Navegacao);
