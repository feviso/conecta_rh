import React, {Component} from 'react';
import { ImageBackground, Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import BackgroundImage from '../assets/fundo.png';
import commonStyles from '../commonStyles';
import AuthInput from '../../src/components/AuthInput';
import axios from 'axios';

import {server, showError, showSuccess} from '../common';

//Restaura o estado inicial tela Login
const initialState = {
  name: 'Valdeir Machado',
  email: '@',
  telefone: '(61)99131-1009',
  password: '123',
  confirmPassword: '',
  stageNew: false, // Verifica telas
};

export default class Auth extends Component {
  
  state = {
    ...initialState,
  };

  LogarOuRegistrar = () => {
    if (this.state.stageNew) {
      this.registrar();
    } else {
      this.logar();
    }
  };

  // Registra nova conta
  registrar = async () => {
    try {
      await axios.post(`${server}/signup`, {
        name: this.state.name,
        telefone: this.state.telefone,
        email: this.state.email,
        password: this.state.password,
        confirmPassword: this.state.confirmPassword,
      });
      showSuccess('Usuario cadastrado!');
      this.setState({...initialState});
    } catch (e) {
      showError(e);
    }
  };

  logar = async () => {
    try {
      const res = await axios.post(`${server}/signin`, {
        email: this.state.email,
        password: this.state.password,
      });

      axios.defaults.headers.common['Authorization'] = `bearer ${res.data.token}`;
      this.props.navigation.navigate('Home', { 'nome': this.state.name});
    } catch (e) {
      showError(e);
    }
  };

  
  
  render() {
    const logout = () => {
      delete axios.defaults.headers.common['Authorization']
      AsyncStorage.removeItem('userData')
      this.props.navigation.navigate()
    }
    const validations = [];

    validations.push(this.state.email && this.state.email.includes('@'));
    validations.push(this.state.password && this.state.password.length >= 3);

    if (this.state.stageNew) {
      validations.push(this.state.name && this.state.name.trim());
      validations.push(this.state.confirmPassword);
      validations.push(this.state.password === this.state.confirmPassword);
    }

    const validForm = validations.reduce((all, v) => all && v);

    return (
      <ImageBackground source={BackgroundImage} style={styles.background}>
        <Text style={styles.title}>Conexão RH</Text>

        <View style={styles.formContainer}>
          <Text style={styles.subtitle}>
            {this.state.stageNew ? 'Crie sua conta' : 'Informe seus dados'}
          </Text>
          {this.state.stageNew && (
            <AuthInput
              icon="user"
              placeholder="Nome"
              value={this.state.name}
              style={styles.input}
              onChangeText={name => this.setState({name})}
            />
          )}

          {this.state.stageNew && (
            <AuthInput
              icon="phone"
              placeholder="Telefone"
              value={this.state.telefone}
              style={styles.input}
              onChangeText={telefone => this.setState({telefone})}
            />
          )}

          <AuthInput
            icon="at"
            placeholder="E-mail"
            value={this.state.email}
            style={styles.input}
            onChangeText={email => this.setState({email})}
          />

          <AuthInput
            icon="lock"
            placeholder="Senha"
            secureTextEntry={true}
            value={this.state.password}
            style={styles.input}
            onChangeText={password => this.setState({password})}
          />

          {this.state.stageNew && (
            <AuthInput
              icon="asterisk"
              placeholder="Confirmar senha"
              secureTextEntry={true}
              value={this.state.confirmPassword}
              style={styles.input}
              onChangeText={confirmPassword => this.setState({confirmPassword})}
            />
          )}
          <TouchableOpacity
            disabled={!validForm}
            onPress={this.LogarOuRegistrar}>
            <View
              style={[
                styles.botton,
                !validForm ? {backgroundColor: '#AAA'} : {},
              ]}>
              <Text style={styles.bottonText}>
                {this.state.stageNew ? 'Registrar' : 'Acessar'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{padding: 10}}
          onPress={() => {
            this.setState({stageNew: !this.state.stageNew});
          }}>
          <Text style={styles.bottonText}>
            {this.state.stageNew
              ? 'Já possue conta?'
              : 'Ainda não possue conta?'}
          </Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontFamily: commonStyles.fontFamily,
    color: commonStyles.colors.secondary,
    fontSize: 45,
    marginBottom: 10,
  },
  subtitle: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10,
  },
  input: {
    marginTop: 10,
    backgroundColor: '#FFF',
  },
  formContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    padding: 20,
    width: '90%',
  },
  botton: {
    backgroundColor: '#8e44ad',
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    borderRadius: 10,
  },
  bottonText: {
    fontFamily: commonStyles.fontFamily,
    color: '#FFF',
    fontSize: 20,
  },
});
