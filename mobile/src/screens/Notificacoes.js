import React, {Component, useState, useEffect} from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Text,
  StyleSheet,
  View,
  TextInput,
  StatusBar,
  Platform,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import AuthInput from '../components/AuthInput';
import {server, showError} from '../common';
import axios from 'axios';
import Modal from 'react-native-modal';

export default function Notificacoes() {
  const [text, setText] = useState('');
  const [state, setState] = useState({data: [], loading: false}); // only one data source
  const {data, loading} = state;

  const fetchAPI = () => {
    //setState({data:[], loading: true});
    return fetch(`${server}/noticias`)
      .then(response => response.json())
      .then(data => {
        console.log(data);
        setState({data, loading: false}); // set only data
      })
      .catch(error => {
        console.error(error);
      });
  };

  useEffect(() => {
    fetchAPI();
  }, []); // use `[]` to avoid multiple side effect

  const filterdData = text // based on text, filter data and use filtered data
    ? data.filter(item => {
        const itemData = item.titulo.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      })
    : data; // on on text, u can return all data
  console.log(data);

  const itemSeparator = () => {
    return (
      <View style={{height: 0.5, width: '100%', backgroundColor: '#000'}} />
    );
  };

  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisibleNoticia, setModalVisibleNoticia] = useState(false);

  return (
    <LinearGradient
      style={styles.gradient}
      colors={['#3c0835', '#61045f', '#61045f', '#5d3357', '#7e5d7a']}>
      <View style={styles.refreshContainer}>
        {/* ==================================================================== */}

        <Modal
          style={styles.modalContent}
          animationType="none"
          transparent={true}
          visible={modalVisibleNoticia}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <TouchableOpacity
            style={{...styles.openButton}}
            onPress={() => {
              setModalVisibleNoticia(!modalVisibleNoticia);
            }}>
            <View style={styles.modalContentIntoNoticiaClose}>
              <Text style={styles.buttonNoticiaClose}>
                <Icon name={'close'} size={20} color={'#7e5d7a'} />
              </Text>
            </View>
          </TouchableOpacity>

          <View style={styles.modalContentInto}>
            <View style={styles.modalContentIntoNoticia}>
              <Text style={styles.buttonNoticia}>
                Prezado Aluno, Com a padronização do uso dos recursos do Google
                para todos os alunos, com espaço de armazenamento ilimitado, com
                acesso as ferramentas do Google Docs, Meeting, Classroom e Chat
                e a possibilidade da conta ser mantida mesmo após o término do
                seu curso, desativamos todas as contas @uniceub.edu.br que eram
                usadas no Microsoft Office 365. Caso você tenha algum documento
                ou arquivo que era armazenado no Microsoft Onedrive desta conta,
                solicite a recuperação temporária da conta enviando um e-mail
                para suporte.ti@uniceub.br até o dia 15/06, para possibilitar a
                transferência dos arquivos para o Google Drive. Após esta data,
                não será mais possível recuperá-la. Lembramos que, este
                procedimento não afeta o seu e-mail institucional
                @sempreceub.com ou qualquer serviço do Google.
              </Text>
            </View>
          </View>
        </Modal>

        {/* ==================================================================== */}

        <Modal
          style={styles.modalContent}
          animationType="none"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.modalContentInto}>
            <TouchableOpacity
              style={{...styles.openButton}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              value={text}
              underlineColorAndroid="transparent"
              onPress={text => setText('')}>
              <View style={styles.button}>
                <Text style={styles.button2}>Todas as Notícias</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{...styles.openButton}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              value={text}
              underlineColorAndroid="transparent"
              onPress={text => setText('CEUB Informa')}>
              <View style={styles.button}>
                <Text style={styles.button2}>CEUB Informa:</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{...styles.openButton}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              value={text}
              underlineColorAndroid="transparent"
              onPress={text => setText('RH Informa')}>
              <View style={styles.button}>
                <Text style={styles.button2}>RH Informa</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{...styles.openButton}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}
              value={text}
              underlineColorAndroid="transparent"
              onPress={text => setText('Evento')}>
              <View style={styles.button}>
                <Text style={styles.button2}>Eventos</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{...styles.openButton}}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <View style={styles.buttonSearch}>
                <Text style={styles.button2}>
                  <Icon name={'search'} size={20} color={'#7e5d7a'} />
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor="#3c0835"
        />
        <AuthInput
          placeholder="Pesquisar..."
          style={styles.input}
          onChangeText={text => setText(text)}
          value={text}
          underlineColorAndroid="transparent"
        />
        <TouchableOpacity
          style={styles.botao}
          onPress={() => {
            setModalVisible(true);
          }}>
          <Icon name={'filter'} size={20} color={'#7e5d7a'} />
        </TouchableOpacity>
      </View>

      {/* <View style={styles.filterContainer}>
        <TouchableOpacity
          value={text}
          underlineColorAndroid="transparent"
          onPress={text => setText('')}>
          <Text style={styles.filter1}>CEUB Informa</Text>
        </TouchableOpacity>
        <TouchableOpacity
          value={text}
          underlineColorAndroid="transparent"
          onPress={text => setText('RH Informa')}>
          <Text style={styles.filter1}>RH Informa</Text>
        </TouchableOpacity>
        <TouchableOpacity
          value={text}
          underlineColorAndroid="transparent"
          onPress={text => setText('Evento')}>
          <Text style={styles.filter1}>Eventos</Text>
        </TouchableOpacity>
      </View> */}

      <Text style={styles.title}>NOTÍCIAS PRA VOCÊ</Text>

      {loading === false ? (
        <FlatList
          data={filterdData}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={itemSeparator}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                setModalVisibleNoticia(true);
              }}>
              <View style={styles.linha}>
                <View style={styles.info}>
                  <Text style={styles.name}>
                    {item.idtipo === 1
                      ? 'Ceub informa:'
                      : item.idtipo === 2
                      ? 'RH informa:'
                      : item.idtipo === 3
                      ? 'Eventos:'
                      : ''}
                  </Text>
                  <Text style={styles.noticia}>{item.titulo}</Text>
                  <Text style={styles.noticia}>{item.conteudo}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      ) : (
        <Text>loading</Text>
      )}
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  gradient: {
    flex: 1,
  },
  refreshContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: Platform.OS === 'ios' ? 40 : 20,
    // marginTop: Platform.OS === 'ios' ? 40 : 20,
    height: 60,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  input: {
    borderRadius: 5,
    width: '94%',
    marginBottom: 0,
    marginTop: 0,
    marginLeft: 12,
    marginRight: -40,
    paddingLeft: 20,
  },
  botao: {
    marginTop: 0,
    marginEnd: 10,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    borderRadius: 4,
  },
  filterContainer: {
    paddingTop: 3,
    paddingBottom: 3,
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
  },
  filter1: {
    padding: 5,
    fontWeight: 'bold',
    color: '#FFF',
    width: 130,
    textAlign: 'center',
    alignItems: 'center',
  },
  filter2: {
    padding: 5,
    fontWeight: 'bold',
    color: '#FFF',
    width: 130,
    textAlign: 'center',
    alignItems: 'center',
  },
  filter3: {
    padding: 5,
    fontWeight: 'bold',
    color: '#FFF',
    width: 130,
    textAlign: 'center',
    alignItems: 'center',
  },
  title: {
    flexDirection: 'row',
    width: '100%',
    height: 60,
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 0,
    borderBottomColor: '#54274e',
    borderBottomWidth: 5,
    paddingTop: 10,
    paddingBottom: 10,
    color: '#FFF',
  },
  linha: {
    marginLeft: 10,
    marginRight: 10,
    padding: 8,
    flexDirection: 'row',
    borderBottomColor: '#54274e',
    borderBottomWidth: 5,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  info: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  name: {
    color: '#FFF',
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 3,
  },
  noticia: {
    color: '#FFF',
    fontSize: 12,
  },
  buttonFilter: {
    flexDirection: 'row',
    width: '100%',
    padding: 9,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'red',
    fontSize: 42,
  },
  button: {
    flexDirection: 'row',
    width: '100%',
    padding: 9,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    fontWeight: 'bold',
    color: 'red',
    fontSize: 42,
    borderWidth: 1,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  button2: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 15,
  },
  buttonNoticiaClose: {
    flexDirection: 'row',
    width: 35,
    height: 35,
    paddingTop: 7,
    borderRadius: 100,
    textAlign: 'center',
    fontSize: 15,
    backgroundColor: '#FFF',
  },
  buttonNoticia: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 15,
  },
  buttonSearch: {
    flexDirection: 'row',
    width: '100%',
    padding: 9,
    margin: 2,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'red',
    fontSize: 42,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  modalContentIntoNoticia: {
    flexDirection: 'row',
    width: '100%',
    padding: 9,
    margin: 3,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    color: 'red',
    fontSize: 42,
    marginTop: 3,
    marginBottom: 3,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  modalContentIntoNoticiaClose: {
    flexDirection: 'row',
    width: '100%',
    paddingRight: 0,
    paddingBottom: 0,
    marginBottom: 0,
    justifyContent: 'flex-end',
    fontWeight: 'bold',
    color: 'red',
    fontSize: 42,
    marginTop: 3,
    marginBottom: 3,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
  modalContentInto: {
    backgroundColor: '#FFF',
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 2,
    paddingRight: 7,
    borderRadius: 4,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 42,
  },
  modalContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    width: '100%',
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 4,
    margin: 0,
    fontSize: 42,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  MainContainer: {
    paddingTop: 50,
    justifyContent: 'center',
    flex: 1,
    margin: 5,
    minHeight: 800,
  },
  row: {
    fontSize: 18,
    padding: 12,
  },
  textInput: {
    textAlign: 'center',
    height: 42,
    borderWidth: 1,
    borderColor: '#009688',
    borderRadius: 8,
    backgroundColor: '#333',
  },
});
