import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AuthInput from '../components/AuthInput';
import {server, showError} from '../common';
import axios from 'axios';

export default class Notificacoes extends Component {
  state = {
    noticias: [],
    desctipo: '',
  };

  componentDidMount = async () => {
    try {
      const res = await axios.get(`${server}/noticias`);
      this.setState({
        noticias: res.data,
        desctipo: res.data.idtipo,
      });
    } catch (err) {
      showError(err);
    }
  };

  render() {
    /* const noticias = [
      {key: 1, tipo: 'UniCEUB Informa:',  noticia: 'Próximos Minicursos da Monitoria da Ciência da Computação - ATUALIZAÇÃO'},
      {key: 2, tipo: 'RH Informa:',       noticia: 'Que após vencimento do boleto, será cobrado 50% de juros diários.'},
      {key: 3, tipo: 'Eventos Informa:',  noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.'},
      {key: 4, tipo: 'RH Informa:',       noticia: 'Que após vencimento do boleto, será cobrado 50% de juros diários.'},
      {key: 5, tipo: 'UniCEUB Informa:',  noticia: 'O Direito Público em tempos de COVID-19 - No dia 15/5, às 10h, o UniCEUB convida você a participar de um importante debate em parceria com o Instituto Max Planck sobre os impactos da pandemia do novo coronavírus nos aspectos legais.'},
      {key: 6, tipo: 'UniCEUB Informa:',  noticia: 'A respeito do coronavirus acesse site abaixo'},
      {key: 7, tipo: 'RH Informa:',       noticia: 'Que após vencimento do boleto, será cobrado 50% de juros diários.'},
      {key: 8, tipo: 'UniCEUB Informa:',  noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.'},
      {key: 9, tipo: 'RH Informa:',       noticia: 'Que após vencimento do boleto, será cobrado 50% de juros diários.'},
]
 */
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor="#61045f"
        />

        <View style={styles.refreshContainer}>
          <AuthInput
            icon="search"
            placeholder="Pesquisar..."
            style={styles.input}
          />

          <TouchableOpacity
            style={styles.botao}
            onPress={this.componentDidMount}>
            {/* <Text style={styles.textBotao}>Gravar</Text> */}
            <Icon name={'undo'} size={30} color={'#FFF'} />
          </TouchableOpacity>
        </View>

        <View style={styles.filterContainer}>
          <TouchableOpacity>
            <Text style={styles.filter1}>CEUB Informa</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.filter2}>RH Informa</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.filter3}>Eventos</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>NOTÍCIAS PRA VOCÊ</Text>
        <FlatList
          data={this.state.noticias}
          renderItem={({item}) => (
            <TouchableOpacity>
              <View style={styles.linha}>
                <View style={styles.info}>
                  <Text style={styles.name}>
                    {item.idtipo === 1
                      ? 'Ceub informa:'
                      : item.idtipo === 2
                      ? 'RH informa:'
                      : item.idtipo === 3
                      ? 'Eventos informa:'
                      : ''}
                  </Text>
                  <Text style={styles.noticia}>{item.descricao}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: '#FFF',
  },
  linha: {
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 2,
  },
  info: {
    paddingTop: 5,
    alignContent: 'center',
  },
  noticia: {
    fontSize: 15,
  },
  name: {
    fontSize: 17,
    fontWeight: 'bold',
    /* color: '#555', */
    color: '#61045f',
  },
  input: {
    borderRadius: 10,
    width: '82%',
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
    backgroundColor: '#61045f',
    width: '97%',
    borderRadius: 5,
    color: '#FFF',
    marginBottom: 10,
  },
  filterContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#61045f',
    marginBottom: 5,
    width: '97%',
    borderRadius: 5,
  },
  refreshContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    /* backgroundColor: '#61045f', */
    marginBottom: 5,
    width: '100%',
    borderRadius: 5,
  },
  filter1: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  filter2: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  filter3: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  botao: {
    marginTop: 5,
    marginEnd: 10,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#61045f',
    borderRadius: 4,
  },
});
