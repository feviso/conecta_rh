import React, {Component} from 'react'
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  StatusBar,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import AuthInput from '../components/AuthInput'
import {server, showError} from '../common'
import axios from 'axios'

export default class Notificacoes extends Component {
  state = {
    noticias: [],
    desctipo: '',
    loading: true,
    descricao: '',
  }

  onChange = (e) => {
    this.setState({...this.state, descricao: e.target.value})
  }
  
  componentDidMount = async () => {
    try {
      const res = await axios.get(`${server}/noticias`)
      this.setState({
        noticias: res.data,
        desctipo: res.data.idtipo,
        loading: false,
      })
    } catch (err) {
      showError(err)
    }
  }
  
  //=========PESQUISA=========//
  refresh = (descricao = '') => {
    const pesquisa = descricao ? `&descricao=${descricao}/` : ''
	  
	  axios.get(`${server}/noticias?${pesquisa}`)
	  .then(resp => this.setState({...this.state, descricao, noticias: resp.data}),
		)
	}
	pesquisar = () => {
    this.refresh(this.state.descricao)
    Alert.alert("Teste", this.state.resp.data)
	}
	//=========FIM PESQUISA=========//

  // ==============================================================
  updateFilter1 = async () => {
    try {
      const res = await axios.get(`${server}/noticias`)
      this.setState(
        {
          noticias: res.data,
          desctipo: res.data.idtipo,
        },
        this.toggleFilter1,
      )
    } catch (err) {
      showError(err)
    }
  }
  updateFilter2 = async () => {
    try {
      const res = await axios.get(`${server}/noticias`)
      this.setState(
        {
          noticias: res.data,
          desctipo: res.data.idtipo,
        },
        this.toggleFilter2,
      )
    } catch (err) {
      showError(err)
    }
  }
  updateFilter3 = async () => {
    try {
      const res = await axios.get(`${server}/noticias`)
      this.setState(
        {
          noticias: res.data,
          desctipo: res.data.idtipo,
        },
        this.toggleFilter3,
      )
    } catch (err) {
      showError(err)
    }
  }

  toggleFilter1 = () => {
    const newNoticias1 = this.state.noticias.filter(item => {
      return item.idtipo === 1
    })
    this.setState({
      noticias: newNoticias1,
    })
  }
  toggleFilter2 = () => {
    const newNoticias2 = this.state.noticias.filter(item => {
      return item.idtipo === 2
    })
    this.setState({
      noticias: newNoticias2,
    })
  }
  toggleFilter3 = () => {
    const newNoticias3 = this.state.noticias.filter(item => {
      return item.idtipo === 3
    })
    this.setState({
      noticias: newNoticias3,
    })
  }
  // ==============================================================

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color="#61045f" />

          <Text>Aguarde, carregando notícias...</Text>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <StatusBar
            barStyle="light-content"
            hidden={false}
            backgroundColor="#61045f"
          />

          <View style={styles.refreshContainer}>
            <AuthInput
              icon="search"
              placeholder="Pesquisar..."
              style={styles.input}
              value={this.state.descricao}
              onChange={this.onChange}
            />

            <TouchableOpacity style={styles.botao} onPress={this.pesquisar}>
              <Icon name={'search'} size={30} color={'#FFF'} />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.botao}
              onPress={this.componentDidMount}>
              <Icon name={'undo'} size={30} color={'#FFF'} />
            </TouchableOpacity>
          </View>

          <View style={styles.filterContainer}>
            <TouchableOpacity onPress={this.updateFilter1}>
              <Text style={styles.filter1}>CEUB Informa</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.updateFilter2}>
              <Text style={styles.filter2}>RH Informa</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.updateFilter3}>
              <Text style={styles.filter3}>Eventos</Text>
            </TouchableOpacity>
          </View>
          <Text style={styles.title}>NOTÍCIAS PRA VOCÊ</Text>
          <FlatList
            data={this.state.noticias}
            renderItem={({item}) => (
              <TouchableOpacity>
                <View style={styles.linha}>
                  <View style={styles.info}>
                    <Text style={styles.name}>
                      {item.idtipo === 1
                        ? 'Ceub informa:'
                        : item.idtipo === 2
                        ? 'RH informa:'
                        : item.idtipo === 3
                        ? 'Eventos informa:'
                        : ''}
                    </Text>
                    <Text style={styles.noticia}>{item.descricao}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id.toString()}
          />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: '#FFF',
  },
  linha: {
    flexDirection: 'row',
    borderBottomColor: '#ccc',
    borderBottomWidth: 2,
  },
  info: {
    paddingTop: 5,
    alignContent: 'center',
  },
  noticia: {
    fontSize: 15,
  },
  name: {
    fontSize: 17,
    fontWeight: 'bold',
    /* color: '#555', */
    color: '#61045f',
  },
  input: {
    borderRadius: 10,
    width: '70%',
    marginBottom: 5,
    marginTop: 5,
    marginLeft: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
    backgroundColor: '#61045f',
    width: '97%',
    borderRadius: 5,
    color: '#FFF',
    marginBottom: 10,
  },
  filterContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#61045f',
    marginBottom: 5,
    width: '97%',
    borderRadius: 5,
  },
  refreshContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    /* backgroundColor: '#61045f', */
    marginBottom: 5,
    width: '100%',
    borderRadius: 5,
  },
  filter1: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  filter2: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  filter3: {
    padding: 10,
    fontWeight: 'bold',
    color: '#FFF',
  },
  botao: {
    marginTop: 5,
    marginEnd: 10,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#61045f',
    borderRadius: 4,
  },
})
