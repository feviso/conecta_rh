import React, {Component} from 'react';
import {
  View, StyleSheet, AsyncStorage, Image, Text, StatusBar, TouchableOpacity, SafeAreaView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient'

export default class Home extends Component {

  render() {
    // #8e44ad

    const {navigation} = this.props;
    //const nomeUsuario = navigation.getParam('nome');

    return (

     <LinearGradient colors={['#20011f', '#61045f', '#61045f' ]}>


      <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
       <StatusBar barStyle="light-content" hidden={false} backgroundColor="#61045f"/>
          <View style={styles.iconBar}>
            <TouchableOpacity style={styles.confIcon} onPress={() =>{navigation.navigate('Notificacoes');}}>
              <Icon name={'bell'} size={30} color={'#FFF'} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {navigation.navigate('Configuracoes'); }}>
              <Icon name="cog" size={30} color="#FFF" />
            </TouchableOpacity>
          </View>
        <View style={styles.containerp}>
          <Image style={styles.logo} source={require('../assets/professor.jpeg')} />
            <Text style={styles.texto}>Sergio Cozzeti</Text>
            <Text style={styles.textoSub}>DRT: 010101-5</Text>
            <Text style={styles.textoSub}>Assistente Técnico - F3</Text>
        </View>
      </View>
        </SafeAreaView>  
        </LinearGradient>
       
    );
  }
}

const styles = StyleSheet.create({
  safeAreaView:{
    height: 250
  },
  container: {
    flex: 1
  },
  containerp: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 450
    },
  iconBar: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    maxHeight: 500,
    padding: 5
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 100,
    borderWidth: 4,
    borderColor: '#FFF',
  },
  texto: {
    fontWeight: "bold",
    fontSize: 20,
    color:'#111'
  },
  textoSub: {
    fontWeight: "bold",
    color:'#111'
  },
  confIcon:{
    marginEnd:10
  }
});

/*
 <LinearGradient colors={['#61045f']}>    
 </LinearGradient>
 */