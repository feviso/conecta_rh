import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Text,
  AsyncStorage,
  TouchableOpacity,
  Image,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Configuracoes extends Component {

  componentDidMount() {
    this.props.navigation.setParams({logout: this._logout.bind(this)});
    this.props.navigation.setParams({tutorial: this._tutorial.bind(this)});
    this.props.navigation.setParams({alterarFoto: this._alterarFoto.bind(this)});
    this.props.navigation.setParams({sgi: this._logarSgi.bind(this)});
  }

  _logout() {
    AsyncStorage.removeItem('app_token');
    this.props.navigation.navigate('Auth');
  }

  _tutorial() {

  }

  _alterarFoto() {

  }

  _logarSgi() {

  }

  render() {
    const {navigation} = this.props;
    const {params = {} } = navigation.state;

    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" hidden={false} backgroundColor="#61045f"/>
        <View style={styles.iconBar}>
          <TouchableOpacity style={styles.confIcon} onPress={() => {this.props.navigation.navigate('Notificacoes')}}>
            <Icon name={'bell'} size={30} color={'#A0A0A0'} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('Configuracoes')}}>
            <Icon name="cog" size={30} color="#A0A0A0" />
          </TouchableOpacity>
        </View>
        <View style={styles.linha}>
          <TouchableOpacity onPress={params.alterarFoto}>   
            <Icon style={styles.icones} name="camera-retro" size={40} color='#FFF' />
          </TouchableOpacity>
          <View style={styles.info}>
            <Text style={styles.email}>- Alterar foto</Text>
            <Text style={styles.email}>Personalize seu App</Text>
          </View>
        </View>
        <View style={styles.linha}>
          <TouchableOpacity onPress={params.tutorial}>   
            <Icon style={styles.icones} name="address-card" size={40} color='#FFF' />
          </TouchableOpacity>
          <View style={styles.info}>

          <TouchableOpacity onPress={params._logarSgi}>   
            <Text style={styles.sgi}>- Usar serviço de login do SGI</Text>
          </TouchableOpacity>
            
            <Text style={styles.email}>- Tutorial</Text>
            <Text style={styles.email}>Aprenda a utilizar seu App</Text>
          </View>
        </View>
        <View style={styles.linha}>
          <TouchableOpacity onPress={params.logout}>   
            <Icon style={styles.icones} name="sign-out" size={40} color='#FFF' />
          </TouchableOpacity>
          <View style={styles.info}>
            <Text style={styles.email}>- Sair</Text>
            <Text style={styles.email}>Sair do Sistema</Text>
          </View>
        </View>

        <View style={styles.containerp}>
          {/*<Image style={styles.logo} source={require('../assets/professor.jpeg')} />*/}
          <Text style={styles.texto}>Sergio Cozzeti</Text>
          <Text style={styles.textoSub}>DRT: 010101-5</Text>
          <Text style={styles.textoSub}>Assistente Técnico - F3</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  containerp: {
    justifyContent: 'center',
    alignItems: 'center',
     marginTop: 10
  },
  confIcon: {
    marginHorizontal: 10,
  },
  iconBar: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    maxHeight: 100,
    padding: 5
  },
  icones: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 10
  },
  texto: {
    fontWeight: 'bold',
    fontSize: 25,
    color: '#111',
  },
  sgi:{ 
    marginBottom: 20,
    color: '#FFF',
    fontWeight: 'bold',

  },
  textoSub: {
    fontWeight: 'bold',
    color: '#111',
  },
  lista: {
    fontSize: 20,
    color: '#34495e',
    padding: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  linha: {
    marginHorizontal: 5,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    backgroundColor: '#61045f',
    borderBottomWidth: 1,
    borderBottomColor: '#FFF',
  },
  avatar: {
    width: 40,
    height: 40,
    borderRadius: 50,
    marginRight: 10,
    alignSelf: 'center',
  },
  info: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    padding: 20
  },
  nome: {
    fontSize: 12,
  },
  email: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FFF'
  },
});
