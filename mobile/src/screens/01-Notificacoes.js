import React, {Component} from 'react';
import {View, StyleSheet, Text, FlatList, StatusBar} from 'react-native';
import AuthInput from '../components/AuthInput';
import LinearGradient from 'react-native-linear-gradient';

import {server, showError} from '../common';
import axios from 'axios';

export default class Notificacoes extends Component {
  state = {
    noticias: [],
  };

  componentDidMount = async () => {
    try {
      const res = await axios.get(`${server}/noticias`);
      this.setState({noticias: res.data});
    } catch (err) {
      showError(err);
    }
  }

  render() {
    /*  const noticias = [
      { key: 'UniCeub Informa 1:', noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.' },
      { key: 'RH Informa 1:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 2:', noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.' },
      { key: 'RH Informa 2:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 3:', noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.' },
      { key: 'RH Informa 3:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 4:', noticia: 'Dados do Uniceub' },
      { key: 'RH Informa 4:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'RH Informa 5:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 5:', noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.' },
      { key: 'UniCeub Informa 6:', noticia: 'Dados do Uniceub' },
      { key: 'RH Informa 6:', noticia: 'A respeito do coronavirus estamos suspendendo as aulas até o final de dezembro e início em janeiro.' },
      { key: 'UniCeub Informa 7:', noticia: 'Dados do Uniceub' },
      { key: 'RH Informa 7', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 8:', noticia: 'Dados do Uniceub' },
      { key: 'UniCeub 1', noticia: 'Dados do Uniceub' },
      { key: 'RH Informa:', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
      { key: 'UniCeub Informa 9:', noticia: 'Dados do Uniceub' },
      { key: 'RH Informa 10', noticia: 'RH informa que apos o vencimento combrar juros de 5%.' },
    ] */

    return (
      <LinearGradient
        colors={[
          '#3c0835', '#3c0835', '#61045f', '#61045f', '#61045f', '#663f62', '#663f62', 
          '#663f62', '#663f62', '#663f62', '#8f748c', '#8f748c','#8f748c'
        ]}>
        {/* <View style={styles.container}> */}
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor="#3c0835"
        />
        <AuthInput
          icon="search"
          placeholder="Pesquisar..."
          style={styles.input}
        />

        <View style={styles.filterContainer}>
          <Text style={styles.filter1}>UniCeub Informa</Text>
          <Text style={styles.filter2}>RH Informa</Text>
          <Text style={styles.filter3}>Eventos</Text>
        </View>

        <Text style={styles.title}>NOTÍCIAS PRA VOCÊ</Text>
        <FlatList 
          data={this.state.noticias} 
            renderItem={({item}) => (
            <View style={styles.linha}>
              <View style={styles.info}>
                <Text style={styles.name}>{item.id.toString()}</Text>
                <Text style={styles.noticia}>{item.descricao}</Text>
                {/* <Text style={styles.noticia}>{item.datacadastro}</Text> */}
              </View>
            </View>
          )}
          keyExtractor={item => item.id.toString()}
        />
        {/* </View> */}
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 10,
    backgroundColor: 'red',
    // marginLeft: 10,
  },
  linha: {
    marginLeft: 10,
    marginRight: 10,
    // height: 73,
    padding: 8,
    flexDirection: 'row',
    borderBottomColor: '#54274e',
    borderBottomWidth: 5,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  info: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  name: {
    color: '#FFF',
    fontSize: 15,
    fontWeight: 'bold',
    marginBottom: 3,
  },
  noticia: {
    color: '#FFF',
    fontSize: 12,
  },
  input: {
    borderRadius: 5,
    // marginLeft: 10,
    margin: 10,
    marginBottom: 0,
    width: '94%',
    // marginBottom: 5
  },
  filterContainer: {
    // fontWeight: "bold",
    // fontSize: 16,
    // textAlign: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // borderRadius: 0,
    // borderBottomColor: '#54274e',
    // borderBottomWidth: 4,
    // color: '#FFF',
    paddingTop: 5,
    paddingBottom: 0,
    // backgroundColor: 'aqua',
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
    // width: '97%',
    // backgroundColor: '#61045f',
  },
  filter1: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // borderRadius: 0,
    borderBottomColor: '#3c0835',
    borderBottomWidth: 4,
    paddingTop: 1,
    paddingBottom: 4,
    color: '#FFF',
    marginLeft: 3,
    marginRight: 3,
    flexDirection: 'row',
    width: 125,
    // backgroundColor: '#61045f',
  },
  filter2: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // borderRadius: 0,
    borderBottomColor: '#54274e',
    borderBottomWidth: 4,
    paddingTop: 1,
    paddingBottom: 4,
    color: '#FFF',
    marginLeft: 3,
    marginRight: 3,
    flexDirection: 'row',
    width: 100,
    // backgroundColor: '#61045f',
  },
  filter3: {
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // borderRadius: 0,
    borderBottomColor: '#54274e',
    borderBottomWidth: 4,
    paddingTop: 1,
    paddingBottom: 4,
    color: '#FFF',
    marginLeft: 3,
    marginRight: 3,
    flexDirection: 'row',
    width: 100,
    // backgroundColor: '#61045f',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 0,
    borderBottomColor: '#54274e',
    borderBottomWidth: 5,
    paddingTop: 10,
    paddingBottom: 10,
    color: '#FFF',
    marginLeft: 10,
    marginRight: 10,
    // width: '97%',
    // backgroundColor: '#61045f',
  },
});
