import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import Routers from './components/Routers'
import Auth from './screens/Auth'
import Notificacoes from './screens/Notificacoes'
import Configuracoes from './screens/Configuracoes'

const minhasRotas = {
  Auth: {
    name: 'Auth',
    screen: Auth,
  },
  Routers: {
    name: 'Routers',
    screen: Routers,
  },
  Notificacoes: {
    name: 'Notificacoes',
    screen: Notificacoes
  },
  Configuracoes: {
    name: 'Configuracoes',
    screen: Configuracoes
  }
}

const meuNavegador = createSwitchNavigator(minhasRotas, {
  //initialRouteName: 'Routers'
  initialRouteName: 'Auth'
});

export default createAppContainer(meuNavegador)
