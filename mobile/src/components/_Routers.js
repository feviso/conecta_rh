import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import {createBottomTabNavigator} from 'react-navigation-tabs'

import Home from '../screens/Home'
import ContraCheques from '../screens/ContraCheques'
import Duvidas from '../screens/Duvidas'
import Oportunidades from '../screens/Oportunidades'
import Vantagens from '../screens/Vantagens'
import Configuracoes from '../screens/Configuracoes'
import Notificacoes from '../screens/Notificacoes'

export default createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarIcon: ({tintColor}) => (
          <Icon name="home" color={tintColor} size={30} />
        )
      }
    },
    Oportunidades: {
      screen: Oportunidades,
      navigationOptions: {
        tabBarLabel: 'Oportunidades',
        tabBarIcon: ({tintColor}) => (
          <Icon name="address-card" color={tintColor} size={30} />
        )
      }
    },
    Vantagens: {
      screen: Vantagens,
      navigationOptions: {
        tabBarLabel: 'Vantagens',
        tabBarIcon: ({tintColor}) => (
          <Icon name="folder-open" color={tintColor} size={30} />
        )
      }
    },
    Duvidas: {
      screen: Duvidas,
      navigationOptions: {
        tabBarLabel: 'RH e Você',
        tabBarIcon: ({tintColor}) => (
          <Icon name="file" color={tintColor} size={30} />
        )
      }
    },
    ContraCheques: {
      screen: ContraCheques,
      navigationOptions: {
        tabBarLabel: 'Contra Cheque',
        tabBarIcon: ({tintColor}) => (
          <Icon name="money" color={tintColor} size={30} />
        )
      }
    }
  },
  {
    initialRouteName: 'Home',
    //order: ['Home', 'Oportunidades', 'Vantagens', 'Duvidas', 'ContraCheques'],
    tabBarOptions: {
      activeTintColor: '#8e44ad',
      shifting: true,
      showLabel: true,
      tabStyle: {
        padding: 2
      }
      //inactiveTintColor: '#CCC',
    }
  }
)
