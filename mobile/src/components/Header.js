import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  ImageBackground,
  TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class ContraCheques extends Component {

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.background}>
          <View style={styles.iconBar}>
            <TouchableOpacity style={styles.confIcon} onPress={() =>{this.props.navigation.navigate('Notificacoes');}}>
              <Icon name={'bell'} size={30} color={'#A0A0A0'} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {this.props.navigation.navigate(''); }}>
              <Icon name="cog" size={30} color="#A0A0A0" />
            </TouchableOpacity>
          </View>
        </ImageBackground>
        <View style={styles.containerp}>
          <Image style={styles.logo} source={require('../assets/notif.png')} />
            <Text style={styles.texto}>Sergio Cozzeti</Text>
            <Text style={styles.textodrt}>DRT: 010101-5</Text>
            <Text style={styles.textof}>Assistente Técnico - F3</Text>
        </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  containerp: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  background: {
    flex: 3,
    maxHeight: 150
  },
  iconBar: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    maxHeight: 200,
    padding: 5
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 100,
  },
  texto: {
    fontWeight: "bold",
    fontSize: 20
  },
  textodrt: {
    fontWeight: "bold"
  },
  textof: {
    fontWeight: 'bold'
  },
  confIcon:{
    marginEnd:10
  }
});
  /*
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('../assets/professor.jpeg')} />
          <Text style={styles.textodrt}>Contra Cheques</Text>
          <Text style={styles.textof}>Assistente Técnico - F3</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconBar: {
    marginTop: Platform.OS === 'ios' ? 30 : 10,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    maxHeight: 200,
    padding: 5
  },
  logo: {
    width: 150,
    height: 150,
    borderRadius: 100,
  },
  texto: {
    fontWeight: "bold",
    fontSize: 20
  },
  textodrt: {
    fontWeight: "300",
    fontSize: 30
  },
  textof: {
    fontWeight: '900'
  }
})*/

/*
<TouchableOpacity style={styles.c1} onPress={() =>{this.props.navigation.navigate('Notificacoes')}}>
                    <Icon name={'bell'} size={30} color={'#800'} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.props.navigation.navigate('Configuracoes')}}>
                    <Icon name="cog" size={30} color="#800" />
                </TouchableOpacity>
*/
