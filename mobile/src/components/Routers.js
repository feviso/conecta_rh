import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { createBottomTabNavigator } from 'react-navigation-tabs'

import Home from '../screens/Home'
import ContraCheques from '../screens/ContraCheques'
import Duvidas from '../screens/Duvidas'
import Oportunidades from '../screens/Oportunidades'
import Vantagens from '../screens/Vantagens'
import Notificacoes from '../screens/Notificacoes'

export default createBottomTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        tabBarLabel: 'Início',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" color={tintColor} size={30} />
        )
      }
    },
    Oportunidades: {
      screen: Oportunidades,
      navigationOptions: {
        tabBarLabel: 'Oportunidades',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="share-alt" color={tintColor} size={26} />
        )
      }
    },
    Vantagens: {
      screen: Vantagens,
      navigationOptions: {
        tabBarLabel: 'Parceiros',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="folder-open" color={tintColor} size={26} />
        )
      }
    },
    Duvidas: {
      screen: Duvidas,
      navigationOptions: {
        tabBarLabel: 'RH e Você',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="file" color={tintColor} size={22} />
        )
      }
    },
    ContraCheques: {
      screen: ContraCheques,
      navigationOptions: {
        tabBarLabel: 'Financeiro',
        tabBarIcon: ({ tintColor }) => (
          <Icon name="money" color={tintColor} size={26} />
        )
      }
    }
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: '#8e44ad',
      shifting: true,

      // Stylo da Barra de Menus inteira
      style: {
        height: 55,
        margin: 0,
        paddingTop: 2,
        paddingBottom: 2
        // maxWidth: 700,
        // display: 'flex',
        // alignItems: "center",
        // borderColor: 'red',
        // borderWidth: 1,
        // justifyContent: 'space-between',
        // width: 10
      },

      showLabel: true
    }
  }
)