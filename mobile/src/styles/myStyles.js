import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4682B4',
  },
  container1: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
    },
  texto: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold',
  },
  texto2: {
    color: '#FFFF00',
    fontSize: 20,
    justifyContent: 'center'
  },
  botao: {
    backgroundColor: '#FFF',
    padding: 10,
    borderRadius: 5,
    justifyContent: 'center',
  },
});

export default styles;
